import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Activities from './app/views/Activities/index.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>CoupleStuff</h2>
        </div>
        <Activities />
      </div>
    );
  }
}

export default App;
