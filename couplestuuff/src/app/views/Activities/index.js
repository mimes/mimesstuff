import React, { Component } from 'react';
import './activity-style.css';

//include the classnames utility--lets you make multiple classnames
var classNames = require('classnames');

//list of activities
var activities = ['Watch a show', 'Watch a documentary', 'Do separate things', 'Consider getting twisted', 'Listen to an audiobook', 'Imbibe, partake and converse', 'Do something with friends', 'Go on a date', 'Find something to do outside-y'];

//this function will get the random activity item
function getRandomActivity(){
	let text = window.$("#activity-text");
	text.fadeOut(function(){
		let randomIndex = Math.round(Math.random() * (activities.length - 1));
		text.text(activities[randomIndex])
		text.slideDown();
	});
	

}

var btnClasses = classNames('btn', 'btn-primary');
class Activities extends Component {
	render() {
		return (
			<div className = "activity-container">
				<input id="rec-btn" type="button" className={btnClasses} value ="COUPLE RECREATION!" onClick = {getRandomActivity}/>
				<p id="activity-text" >{getRandomActivity()}</p>
			</div>
		);
	}
}
export default Activities;